import $ from "jquery";

const axios = require('axios');

    axios.get('https://restcountries.eu/rest/v2/name/italy')
    .then((response) => {
        console.log(response.data)
        $('#form1').on('submit', (e) => {
            $('#form1').find('button').hide();
            e.preventDefault('#form1')
            let form = new FormData(e.target);
            console.log(form.get('rep'));
            if (form.get('rep') === "italie"){
                $('<h2></h2>').appendTo('#firstquestion').text('Bonne réponse !').addClass('text-success')
                $('<h4></h4>').appendTo('#firstquestion').text('Je péfère les spaghettis à la bolo perso...')
                $('<ul></ul>').appendTo('#firstquestion')
                $('<li></li>').appendTo('ul').text(response.data[0].name)
                $('<li></li>').appendTo('ul').text(response.data[0].alpha3Code)
                $('<li></li>').appendTo('ul').text(response.data[0].timezones[0])
                $('<li></li>').appendTo('ul').text(response.data[0].capital)
            }
            else {
                $('<h2></h2>').appendTo('#firstquestion').text('Mauvaise réponse !').addClass('text-danger')
                $('<h4></h4>').appendTo('#firstquestion').text('Tant pis... tente ta chance sur les autres questions..')
                $('<h6></h6>').appendTo('#firstquestion').text('Plât : Spaghetti encre de seiche').addClass('text-bold')
                $('<ul></ul>').appendTo('#firstquestion')
                $('<li></li>').appendTo('ul').text(response.data[0].name)
                $('<li></li>').appendTo('ul').text(response.data[0].alpha3Code)
                $('<li></li>').appendTo('ul').text(response.data[0].timezones[0])
                $('<li></li>').appendTo('ul').text(response.data[0].capital)

            }
        })
       
    })
    .catch((error) => {
       console.log(error)
    })
        